console.log("=== nomor 1 ====")
console.log("\nLooping pertama\n")
var i = 2;
while(i<=20){
    console.log(i + " - I love coding")
    i+=2
}

console.log("\nLooping kedua\n")
i = 20
while(i>=2) {
    console.log(i + " - I will become a mobile developer")
    i-=2
}

console.log("\n====nomor 2====\n")
var santai = " - santai"
var berkualitas = " -berkualitas"
var lovecoding = " -I Love coding"

for(i = 1; i <= 20; i++){
    if(i % 2 !=1) {
        console.log(i + berkualitas)
    } else if(i % 3 == 0) {
        console.log(i + lovecoding)
    } else {
        console.log(i + santai)
    }
}

console.log("\n===nomor 3=====\n")
var k = 1
var j = 1
var panjang = 8;
var lebar = 4;
var pagar = " "

while(j <= lebar) {
    while (k <= panjang) {
        pagar += "#"
        k++
    }
    console.log(pagar)
    pagar = " " 
    k=1
    j++
}

console.log("\n====nomor 4====\n")

i = 1;
j = 1;
var alas = 7;
var tinggi = 7;
pagar = "";

for(i = 1; i <=tinggi; i++){
    for(j = 1; j <=i; j++){
        pagar += "#"
    }
    console.log(pagar)
    pagar = ""
}

console.log("\nNormor 5======\n")

i = 1
j = 1
panjang = 8
lebar = 8
var papan = ""

for(j = 1; j <= lebar; j++) {
    if(j%2 ==1) {
        for(i=1; i <= panjang; i++) {
            if(i%2 == 1) {
                papan+=" "
            } else{
                papan += "#"
            }
        }
    } else {
        for(i =1; i <= panjang; i++) {
            if(i%2 == 1) {
                papan +="#"
            } else {
                papan +=" "
            }
        }
    } console.log(papan)
    papan = ""
}