// Start Number 1 A

console.log("\n==== number 1 A ======\n")

var word = "JavaScript"
var second = "is"
var third = "awesome"
var fourth = "and"
var fifth = "I"
var sixth = "love"
var seventh = "it"

var stringGabung = word + " " + second + " " + third + " " + fourth + " " + fifth + " " + sixth + " " + seventh
console.log(stringGabung)

// End of Number 1 A

// Start Number 2 A

console.log("\n\n==== number 2 A =====\n")

var sentence = "I am going to be React Native Developer"
var firstWord = sentence[0]
var secondWord = sentence[2] + sentence[3]
var thirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9]
var fourthWord = sentence[11] + sentence[12]
var fifthWord = sentence[14] + sentence[15]
var sixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21]
var seventhWord = sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28]
var eighthWord = sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38]

console.log("First word: " + firstWord)
console.log("Second word: " + secondWord)
console.log("Third word: " + thirdWord)
console.log("Fourth word: " + fourthWord)
console.log("Fifth word: " + fifthWord)
console.log("Sixth word: " + sixthWord)
console.log("Seventh word: " + seventhWord)
console.log("Eighth word: " + eighthWord)

// End of number 2 A


// start of number 3 A
console.log("\n\n==== number 3 A ======\n")

var sentence2 = "wow JavaScript is so cool"

var firstWord2 = sentence2.substring(0,3)
var secondWord2 = sentence2.substring(4,14)
var thirdWord2 = sentence2.substring(15,17)
var fourthWord2 = sentence2.substring(18,20)
var fifthWord2 = sentence2.substring(21,25)

console.log("First word: " + firstWord2)
console.log("Second Word: " + secondWord2)
console.log("Third word: " + thirdWord2)
console.log("Fourth word: " + fourthWord2)
console.log("Fifth word: " + fifthWord2)

// End of number 3 A

// Start of number 4 A
console.log("\n\n===== number 4 A======\n")
var sentence3 = "wow JavaScript is so cool"

var firstWord3 = sentence3.substring(0, 3)
var secondWord3 = sentence3.substring(4, 14)
var thirdWord3 = sentence3.substring(15, 17)
var fourthWord3 = sentence3.substring(18,20)
var fifthWord3 = sentence3.substring(21,25)

var firstWordLength = firstWord3.length
var secondWordLength = secondWord3.length
var thirdWordLength = thirdWord3.length
var fourthWordLength = fourthWord3.length
var fifthWordLength = fifthWord3.length

console.log("First word: " + firstWord3 + ", with length: " + firstWordLength)
console.log("Second word: " + secondWord3 + ", with length: " + secondWordLength)
console.log("Third word: " + thirdWord3 + ", with length: " + thirdWordLength)
console.log("Fourth word: " + fourthWord3 + ", with length: " + fourthWordLength)
console.log("Fifth word: " + fifthWord3 + ", with length: " + fifthWordLength)
// End of number 4 A







